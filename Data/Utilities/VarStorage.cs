using System;

namespace FlatManagement.Data.Utilities {
    public sealed class VarStorage {
        public int VarStorageId { get; set; }
        /* SINGLETON RELATED STAFF */
        private static volatile VarStorage instance;
        private static Object syncRootObject = new Object();
        private VarStorage() { }
        public static VarStorage Instance {
            get {
                if (instance == null) {
                    lock(syncRootObject) {
                        if (instance == null) {
                            instance = new VarStorage();
                        }
                    }
                }
                return instance;
            }
        }

        /* VARIABLES */
        public int ColorNormal { get; set; } = 0xF3FFA8;
        public int ColorImportant { get; set; } = 0xFF0000;
        public int MaxNoteLength { get; set; } = 250;
    }
}