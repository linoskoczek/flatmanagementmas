using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlatManagement.Data.Shopping {
    public class ShoppingList {
        public int ShoppingListId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsFinished { get; set; } = false;
        public int FlatId { get; set; }
        public Flat Flat { get; set; }
        public ICollection<Item> Items { get; set; }

        public static List<Item> CurrentlyNeededItems() {
            throw new NotImplementedException();
        }
    }
}