using System.ComponentModel.DataAnnotations;

namespace FlatManagement.Data.Shopping {
    public class Item {
        public int ItemId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Name { get; set; }
        public int Pieces { get; set; } = 1;
        public bool isBought { get; set; } = false;
        public int ShoppingListId { get; set; }
        public ShoppingList ShoppingList { get; set; }
    }
}