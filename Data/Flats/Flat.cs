using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FlatManagement.Data.Bills;
using FlatManagement.Data.Notes;
using FlatManagement.Data.Shopping;
using FlatManagement.Data.Tasking;
using FlatManagement.Data.User;

namespace FlatManagement.Data {
    public class Flat {
        public int FlatId { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "{0} length must be smaller than {1}", MinimumLength = 0)]
        public string Address { get; set; }

        public virtual ICollection<Membership> Memberships { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<ShoppingList> ShoppingLists { get; set; }
        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<FlatTask> FlatTasks { get; set; }
    }
}