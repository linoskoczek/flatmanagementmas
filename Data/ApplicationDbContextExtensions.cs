using System;
using System.Collections.Generic;
using FlatManagement.Data.Tasking;
using Microsoft.EntityFrameworkCore;

namespace FlatManagement.Data {
    public static class ApplicationDbContextExtensions {
        public static void Seed(this ModelBuilder modelBuilder) {
            var flat1 = new Flat {
                FlatId = 111,
                Name = "Example flat 1",
                Address = "Rakowiecka 5/13"
            };

            var flattask1 = new FlatTask {
                FlatTaskId = 1243,
                Deadline = DateTime.Now.AddDays(5),
                Name = "Clean the kitchen!",
                Description = "There's such a mess...",
                FlatId = flat1.FlatId,
                IsProgressive = true,
                Progress = 0
            };

            var flattask2 = new FlatTask {
                FlatTaskId = 124322,
                Deadline = DateTime.Now.AddDays(3),
                Name = "Return cat to our neighbour",
                Description = "Return cat to our neighbour",
                FlatId = flat1.FlatId,
                IsProgressive = true,
                Progress = 0
            };

            modelBuilder.Entity<Flat>().HasData(
                flat1,
                new Flat {
                    FlatId = 222,
                    Name = "Example flat 2",
                }
            );

            modelBuilder.Entity<FlatTask>().HasData(
                flattask1,
                flattask2
            );
        }
    }
}