using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FlatManagement.Data.Utilities;

namespace FlatManagement.Data.Notes {
    public class Note {
        public int NoteId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string NoteContent { get; set; }
        public bool IsImportant { get; set; } = false;
        public bool IsContactNote { get; set; } = false;
        private Note() { }

        public Note(string noteContent, bool isImportant, bool isContactNote, string email, string phone) {
            NoteContent = noteContent;
            IsImportant = isImportant;
            IsContactNote = isContactNote;
            Email = email;
            Phone = phone;
        }

        [EmailAddress]
        public string Email {
            get {
                if (!IsContactNote) throw new Exception("This note is not a contact note!");
                else return email;
            }
            set {
                if (!IsContactNote) throw new Exception("This note is not a contact note!");
                else email = value;
            }
        }

        [EmailAddress]
        private string email { get; set; }

        [Phone]
        public string Phone {
            get {
                if (!IsContactNote) throw new Exception("This note is not a contact note!");
                else return phone;
            }
            set {
                if (!IsContactNote) throw new Exception("This note is not a contact note!");
                else phone = value;
            }
        }

        [Phone]
        private string phone { get; set; }
        public int FlatId { get; set; }
        public Flat Flat { get; set; }

        [NotMapped]
        public int Color {
            get => IsImportant ? VarStorage.Instance.ColorImportant : VarStorage.Instance.ColorNormal;
            set {
                if (IsImportant) {
                    VarStorage.Instance.ColorImportant = value;
                } else {
                    VarStorage.Instance.ColorNormal = value;
                }
            }
        }
    }
}