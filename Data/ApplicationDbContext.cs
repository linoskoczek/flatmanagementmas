﻿using System;
using System.Collections.Generic;
using System.Text;
using FlatManagement.Data.Bills;
using FlatManagement.Data.Notes;
using FlatManagement.Data.Shopping;
using FlatManagement.Data.Tasking;
using FlatManagement.Data.User;
using FlatManagement.Data.Utilities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FlatManagement.Data {
    public class ApplicationDbContext : IdentityDbContext {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }
        public DbSet<FlatUser> FlatUsers { get; set; }
        public DbSet<Membership> Memberships { get; set; }
        public DbSet<BillPayment> BillPayments { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ShoppingList> ShoppingLists { get; set; }
        public DbSet<FlatTask> FlatTasks { get; set; }
        public DbSet<Flat> Flats { get; set; }
        public DbSet<VarStorage> VarStorages { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            builder.Entity<GatheredBill>();
            builder.Entity<MonthlyBill>();
            builder.Entity<MonthlyGatheredBill>();

            builder.Entity<Bill>()
                .HasOne<Flat>(m => m.Flat)
                .WithMany(m => m.Bills)
                .HasForeignKey(m => m.BillId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Membership>().HasKey(m => new { m.FlatUserId, m.FlatId });
            builder.Entity<BillPayment>().HasKey(m => new { m.FlatUserId, m.BillPaymentId });

            builder.Seed();

            base.OnModelCreating(builder);
        }
    }
}