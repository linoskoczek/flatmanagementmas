﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FlatManagement.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Surname",
                table: "AspNetUsers",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "Flats",
                columns: table => new
                {
                    FlatId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 60, nullable: false),
                    Address = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flats", x => x.FlatId);
                });

            migrationBuilder.CreateTable(
                name: "VarStorages",
                columns: table => new
                {
                    VarStorageId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ColorNormal = table.Column<int>(nullable: false),
                    ColorImportant = table.Column<int>(nullable: false),
                    MaxNoteLength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VarStorages", x => x.VarStorageId);
                });

            migrationBuilder.CreateTable(
                name: "Bill",
                columns: table => new
                {
                    BillId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 400, nullable: false),
                    Cost = table.Column<double>(nullable: false),
                    Deadline = table.Column<DateTime>(nullable: false),
                    FlatId = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    DeadlineDayOfMonth = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bill", x => x.BillId);
                    table.ForeignKey(
                        name: "FK_Bill_Flats_BillId",
                        column: x => x.BillId,
                        principalTable: "Flats",
                        principalColumn: "FlatId",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "FlatTasks",
                columns: table => new
                {
                    FlatTaskId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 60, nullable: false),
                    Description = table.Column<string>(maxLength: 250, nullable: false),
                    Deadline = table.Column<DateTime>(nullable: false),
                    IsProgressive = table.Column<bool>(nullable: false),
                    Progress = table.Column<int>(nullable: false),
                    FlatId = table.Column<int>(nullable: false),
                    FlatUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlatTasks", x => x.FlatTaskId);
                    table.ForeignKey(
                        name: "FK_FlatTasks_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "FlatId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlatTasks_AspNetUsers_FlatUserId",
                        column: x => x.FlatUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Memberships",
                columns: table => new
                {
                    FlatUserId = table.Column<string>(nullable: false),
                    FlatId = table.Column<int>(nullable: false),
                    Role = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Memberships", x => new { x.FlatUserId, x.FlatId });
                    table.ForeignKey(
                        name: "FK_Memberships_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "FlatId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Memberships_AspNetUsers_FlatUserId",
                        column: x => x.FlatUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Notes",
                columns: table => new
                {
                    NoteId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NoteContent = table.Column<string>(maxLength: 400, nullable: false),
                    IsImportant = table.Column<bool>(nullable: false),
                    IsContactNote = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    FlatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notes", x => x.NoteId);
                    table.ForeignKey(
                        name: "FK_Notes_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "FlatId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShoppingLists",
                columns: table => new
                {
                    ShoppingListId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsFinished = table.Column<bool>(nullable: false),
                    FlatId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShoppingLists", x => x.ShoppingListId);
                    table.ForeignKey(
                        name: "FK_ShoppingLists_Flats_FlatId",
                        column: x => x.FlatId,
                        principalTable: "Flats",
                        principalColumn: "FlatId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillPayments",
                columns: table => new
                {
                    BillPaymentId = table.Column<int>(nullable: false),
                    FlatUserId = table.Column<string>(nullable: false),
                    Value = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    BillId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillPayments", x => new { x.FlatUserId, x.BillPaymentId });
                    table.ForeignKey(
                        name: "FK_BillPayments_Bill_BillId",
                        column: x => x.BillId,
                        principalTable: "Bill",
                        principalColumn: "BillId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BillPayments_AspNetUsers_FlatUserId",
                        column: x => x.FlatUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    ItemId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Pieces = table.Column<int>(nullable: false),
                    isBought = table.Column<bool>(nullable: false),
                    ShoppingListId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.ItemId);
                    table.ForeignKey(
                        name: "FK_Items_ShoppingLists_ShoppingListId",
                        column: x => x.ShoppingListId,
                        principalTable: "ShoppingLists",
                        principalColumn: "ShoppingListId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Flats",
                columns: new[] { "FlatId", "Address", "Name" },
                values: new object[] { 111, "Rakowiecka 5/13", "Example flat 1" });

            migrationBuilder.InsertData(
                table: "Flats",
                columns: new[] { "FlatId", "Address", "Name" },
                values: new object[] { 222, null, "Example flat 2" });

            migrationBuilder.InsertData(
                table: "FlatTasks",
                columns: new[] { "FlatTaskId", "Deadline", "Description", "FlatId", "FlatUserId", "IsProgressive", "Name", "Progress" },
                values: new object[] { 1243, new DateTime(2019, 6, 22, 18, 33, 45, 876, DateTimeKind.Local).AddTicks(1997), "There's such a mess...", 111, null, true, "Clean the kitchen!", 0 });

            migrationBuilder.InsertData(
                table: "FlatTasks",
                columns: new[] { "FlatTaskId", "Deadline", "Description", "FlatId", "FlatUserId", "IsProgressive", "Name", "Progress" },
                values: new object[] { 124322, new DateTime(2019, 6, 20, 18, 33, 45, 876, DateTimeKind.Local).AddTicks(6114), "Return cat to our neighbour", 111, null, true, "Return cat to our neighbour", 0 });

            migrationBuilder.CreateIndex(
                name: "IX_BillPayments_BillId",
                table: "BillPayments",
                column: "BillId");

            migrationBuilder.CreateIndex(
                name: "IX_FlatTasks_FlatId",
                table: "FlatTasks",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_FlatTasks_FlatUserId",
                table: "FlatTasks",
                column: "FlatUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_ShoppingListId",
                table: "Items",
                column: "ShoppingListId");

            migrationBuilder.CreateIndex(
                name: "IX_Memberships_FlatId",
                table: "Memberships",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_Notes_FlatId",
                table: "Notes",
                column: "FlatId");

            migrationBuilder.CreateIndex(
                name: "IX_ShoppingLists_FlatId",
                table: "ShoppingLists",
                column: "FlatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BillPayments");

            migrationBuilder.DropTable(
                name: "FlatTasks");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Memberships");

            migrationBuilder.DropTable(
                name: "Notes");

            migrationBuilder.DropTable(
                name: "VarStorages");

            migrationBuilder.DropTable(
                name: "Bill");

            migrationBuilder.DropTable(
                name: "ShoppingLists");

            migrationBuilder.DropTable(
                name: "Flats");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Surname",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");
        }
    }
}
