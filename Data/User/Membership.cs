namespace FlatManagement.Data.User {
    public class Membership {
        /// <summary>
        /// Role can take only two values:
        /// 0 - means that FlatUser is a regular user of referenced flat
        /// 1 - means that FlatUser is an administrator of referenced flat 
        /// </summary>
        private int role;
        public int Role {
            get => role;
            set {
                if (value == 1) role = 1;
                else role = 0;
            }
        }
        public string FlatUserId { get; set; }
        public FlatUser FlatUser { get; set; }
        public int FlatId { get; set; }
        public Flat Flat { get; set; }
    }
}