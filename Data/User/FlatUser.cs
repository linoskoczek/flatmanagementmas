using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FlatManagement.Data.Bills;
using Microsoft.AspNetCore.Identity;

namespace FlatManagement.Data.User {
    public class FlatUser : IdentityUser {
        [Required]
        [PersonalData]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [PersonalData]
        [StringLength(100, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Surname { get; set; }

        public virtual ICollection<Membership> Memberships { get; set; }
        public virtual ICollection<BillPayment> BillPayments { get; set; }

        public override string ToString() {
            return Name + " " + Surname;
        }
    }
}