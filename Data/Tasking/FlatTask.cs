using System;
using System.ComponentModel.DataAnnotations;
using FlatManagement.Data.User;

namespace FlatManagement.Data.Tasking {
    public class FlatTask {
        public int FlatTaskId { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "Name cannot be longer than {1} characters")]
        public string Name { get; set; }

        [Required]
        [StringLength(250, ErrorMessage = "{0} length must be between {2} and {1}", MinimumLength = 3)]
        public string Description { get; set; }
        public DateTime Deadline { get; set; } = DateTime.Now.AddDays(1);
        public bool IsProgressive { get; set; } = false;
        /// <summary>
        /// For normal tasks (IsProgressive == false):
        /// -1: TODO
        /// -2: DOING
        /// -3: DONE
        /// For progressive tasks (IsProgressive == true):
        /// range from 0 to 100 in percents
        /// </summary>
        [Range(-3, 100)]
        public int Progress {
            get;
            set;
        }
        //     get => progress;
        //     set {
        //         if ((IsProgressive && value < 0) || (!IsProgressive && value >= 0)) 
        //             throw new ArgumentException("Wrong progress value for this type of FlatTask!");
        //     }
        // }

        // [Range(-3, 100)]
        // private int progress { get; set; }
        public int FlatId { get; set; }
        public Flat Flat { get; set; }
        public string FlatUserId { get; set; }
        public FlatUser FlatUser { get; set; }
    }
}