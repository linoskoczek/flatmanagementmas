using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FlatManagement.Data.Bills {
    public abstract class Bill {
        public int BillId { get; set; }

        [Required]
        [StringLength(400, ErrorMessage = "{0} length must be at most {1}")]
        public string Description { get; set; }
        public double Cost { get; set; }
        public DateTime Deadline { get; set; }
        public int FlatId { get; set; }
        public Flat Flat { get; set; }
        public virtual ICollection<BillPayment> BillPayments { get; set; }

        protected Bill() { }

        public Bill(string description, double cost, DateTime deadline, Flat flat) {
            Description = description;
            Cost = cost;
            Deadline = deadline;
            Flat = flat;
            FlatId = flat.FlatId;
        }

        public int DaysLeftToPay() {
            throw new NotImplementedException();
        }
    }
}