using System;
using System.Collections.Generic;
using FlatManagement.Data.User;

namespace FlatManagement.Data.Bills {
    public class MonthlyGatheredBill : MonthlyBill, IGatheredBill {
        private MonthlyGatheredBill() { }

        public MonthlyGatheredBill(string description, double cost, DateTime deadline, Flat flat, int deadlineDayOfMonth) : base(description, cost, deadline, flat, deadlineDayOfMonth) { }

        public List<FlatUser> WhoDidNotPay() {
            throw new System.NotImplementedException();
        }
    }
}