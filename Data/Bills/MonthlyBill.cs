using System;
using System.ComponentModel.DataAnnotations;

namespace FlatManagement.Data.Bills {
    public class MonthlyBill : Bill {
        [Range(1, 28, ErrorMessage = "Deadline day must be between {2} and {1}")]
        public int DeadlineDayOfMonth { get; set; }
        public double LeftToPayThisMonth() {
            throw new NotImplementedException();
        }

        protected MonthlyBill() { }

        public MonthlyBill(string description, double cost, DateTime deadline, Flat flat, int deadlineDayOfMonth) : base(description, cost, deadline, flat) {
            DeadlineDayOfMonth = deadlineDayOfMonth;
        }
    }
}