using System;
using FlatManagement.Data.User;

namespace FlatManagement.Data.Bills {
    public class BillPayment {
        public int BillPaymentId { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public int BillId { get; set; }
        public Bill Bill { get; set; }
        public string FlatUserId { get; set; }
        public FlatUser FlatUser { get; set; }

        private BillPayment() { }
        public BillPayment(Bill bill, FlatUser flatUser, double value) {
            Bill = bill;
            BillId = bill.BillId;
            FlatUser = flatUser;
            FlatUserId = flatUser.Id;
            Date = DateTime.Now.Date;
        }
    }
}