using System.Collections.Generic;
using FlatManagement.Data.User;

namespace FlatManagement.Data.Bills {
    public interface IGatheredBill {
        List<FlatUser> WhoDidNotPay();
    }
}