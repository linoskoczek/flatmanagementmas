using System;
using System.Collections.Generic;
using FlatManagement.Data.User;

namespace FlatManagement.Data.Bills
{
    public class GatheredBill : Bill, IGatheredBill
    {

        private GatheredBill() {}
        
        public GatheredBill(string description, double cost, DateTime deadline, Flat flat) : base(description, cost, deadline, flat)
        {
        }

        public List<FlatUser> WhoDidNotPay()
        {
            throw new System.NotImplementedException();
        }
    }
}