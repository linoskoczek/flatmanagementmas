using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlatManagement.Data;
using FlatManagement.Data.User;
using FlatManagement.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FlatManagement.Repositories {
    public class FlatRepository : IFlatRepository {
        private readonly ApplicationDbContext _context;
        public FlatRepository(ApplicationDbContext context) {
            _context = context;
        }

        public async void AddFlat(Flat flat) {
            await _context.Flats.AddAsync(flat);
            await _context.SaveChangesAsync();
        }

        public async Task<List<Flat>> FindAllAsync() {
            return await _context.Flats.ToListAsync();
        }

        public async Task<IList<Flat>> FindForUserAsync(FlatUser user) {
            return await _context.Flats
                .Include(f => f.Memberships)
                .Where(f => f.Memberships.Any(m => m.FlatUserId == user.Id))
                .ToListAsync();
        }

        public async Task<bool> RemoveAsync(Flat flat) {
            var found = _context.Flats.Where(t => t.FlatId == flat.FlatId).FirstOrDefault();
            if (found != null) {
                _context.Flats.Remove(found);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}