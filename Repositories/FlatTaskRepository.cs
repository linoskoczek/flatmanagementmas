using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FlatManagement.Data;
using FlatManagement.Data.Tasking;
using FlatManagement.Data.User;
using FlatManagement.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FlatManagement.Repositories {
    public class FlatTaskRepository : IFlatTaskRepository {
        private ApplicationDbContext _context;
        public FlatTaskRepository(ApplicationDbContext context) {
            _context = context;
        }

        public async Task<bool> AddAsync(FlatTask task) {
            await _context.FlatTasks.AddAsync(task);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IList<FlatTask>> FindAllAsync() {
            return await _context.FlatTasks
                .Include(t => t.FlatUser)
                .ToListAsync();
        }

        public async Task<IList<FlatTask>> FindForUserAsync(FlatUser user) {
            return await _context.FlatTasks.Where(t => t.FlatUser.Id == user.Id).ToListAsync();
        }

        public async Task<bool> RemoveAsync(FlatTask task) {
            var found = await FindTaskAsync(task.FlatTaskId);
            if (found != null) {
                _context.FlatTasks.Remove(found);
                await _context.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateTaskAsync(FlatTask task) {
            var foundTask = await FindTaskAsync(task.FlatTaskId);
            if (foundTask == null) return false;
            var foundFlatUser = await FindUserByIdAsync(task.FlatUserId);
            if (foundFlatUser == null) return false;
            task.FlatUser = foundFlatUser;
            if ((foundTask.IsProgressive && task.Progress < 0) || (!foundTask.IsProgressive && task.Progress >= 0)) return false;
            var flatmates = await FindFlatMatesAsync(foundTask.FlatId);
            if (flatmates.Count(u => u.Id == task.FlatUserId) == 0) return false;

            CopyTaskProperties(task, foundTask);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<FlatUser> FindUserByIdAsync(string flatUserId) {
            return await _context.FlatUsers.FirstOrDefaultAsync(u => u.Id == flatUserId);
        }

        public async Task<List<FlatUser>> FindFlatMatesAsync(int FlatId) {
            return await _context.FlatUsers.Where(u => u.Memberships.Any(m => m.FlatId == FlatId)).ToListAsync();
        }

        public async Task<int> FindFlatIdAsync(FlatTask task) {
            var found = await FindTaskAsync(task.FlatTaskId);
            if (found == null) throw new Exception("Flat not found!");
            return found.FlatId;
        }

        public async Task<FlatTask> FindTaskAsync(int id) {
            return await _context.FlatTasks.Where(t => t.FlatTaskId == id).FirstOrDefaultAsync();
        }

        private void CopyTaskProperties(FlatTask taskFrom, FlatTask taskTo) {
            taskTo.Progress = taskFrom.Progress;
            taskTo.Name = taskFrom.Name;
            taskTo.Description = taskFrom.Description;
            taskTo.Deadline = taskFrom.Deadline;
            taskTo.FlatUser = taskFrom.FlatUser;
            taskTo.FlatUserId = taskFrom.FlatUserId;
        }

        public async Task<bool> ChangeTaskTypeAsync(int id) {
            var task = await FindTaskAsync(id);
            if (task == null) return false;
            task.IsProgressive = !task.IsProgressive;
            task.Progress = ConvertProgress(task.Progress);
            await _context.SaveChangesAsync();
            return true;
        }

        public int ConvertProgress(int progress) {
            switch (progress) {
                case 0:
                    return -1;
                case 100:
                    return -3;
                case -1:
                    return 0;
                case -2:
                    return 50;
                case -3:
                    return 100;
                default:
                    return -2;
            }
        }

        public async Task<bool> MarkFlatTaskAsDone(int id) {
            var task = await FindTaskAsync(id);
            if (task == null) return false;
            if (task.IsProgressive) task.Progress = 100;
            else task.Progress = ConvertProgress(100);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}