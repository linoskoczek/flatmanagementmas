using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using FlatManagement.Data.Tasking;
using FlatManagement.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace FlatManagement.Repositories {
    public class UserUtility : IUserUtility {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IFlatTaskRepository _flatTaskRepository;

        public UserUtility(UserManager<IdentityUser> userManager, IFlatTaskRepository flatTaskRepository) {
            _userManager = userManager;
            _flatTaskRepository = flatTaskRepository;
        }

        public async Task<string> GetUserIdAsync(System.Security.Claims.ClaimsPrincipal user) {
            var currentUser = await _userManager.GetUserAsync(user);
            return currentUser.Id;
        }

        public async Task<bool> IsAuthorizedToAsync(string UserId, FlatTask task) {
            var flatId = await _flatTaskRepository.FindFlatIdAsync(task);
            var flatMates = await _flatTaskRepository.FindFlatMatesAsync(flatId);
            return flatMates.Where(u => u.Id == UserId).Count() > 0;
        }

        public async Task<bool> IsAuthorizedToAsync(ClaimsPrincipal user, FlatTask task) {
            return await IsAuthorizedToAsync(await GetUserIdAsync(user), task);
        }
    }
}