using System.Collections.Generic;
using System.Threading.Tasks;
using FlatManagement.Data;
using FlatManagement.Data.User;

namespace FlatManagement.Repositories.Interfaces {
    public interface IFlatRepository {
        void AddFlat(Flat flat);

        Task<List<Flat>> FindAllAsync();

        Task<IList<Flat>> FindForUserAsync(FlatUser user);

        Task<bool> RemoveAsync(Flat flat);
    }
}