using System.Collections.Generic;
using System.Threading.Tasks;
using FlatManagement.Data.Tasking;
using FlatManagement.Data.User;

namespace FlatManagement.Repositories.Interfaces {
    public interface IFlatTaskRepository {
        Task<bool> AddAsync(FlatTask task);
        Task<bool> RemoveAsync(FlatTask task);
        Task<bool> UpdateTaskAsync(FlatTask task);
        Task<IList<FlatTask>> FindAllAsync();
        Task<FlatTask> FindTaskAsync(int id);
        Task<IList<FlatTask>> FindForUserAsync(FlatUser user);
        Task<List<FlatUser>> FindFlatMatesAsync(int FlatId);
        Task<FlatUser> FindUserByIdAsync(string flatUserId);
        Task<int> FindFlatIdAsync(FlatTask task);
        Task<bool> ChangeTaskTypeAsync(int id);
        int ConvertProgress(int progress);
        Task<bool> MarkFlatTaskAsDone(int id);
    }
}