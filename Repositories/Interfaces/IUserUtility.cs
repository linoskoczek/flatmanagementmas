using System.Threading.Tasks;
using FlatManagement.Data.Tasking;

namespace FlatManagement.Repositories.Interfaces {
    public interface IUserUtility {
        Task<string> GetUserIdAsync(System.Security.Claims.ClaimsPrincipal user);
        Task<bool> IsAuthorizedToAsync(string UserId, FlatTask task);
        Task<bool> IsAuthorizedToAsync(System.Security.Claims.ClaimsPrincipal user, FlatTask task);
    }
}