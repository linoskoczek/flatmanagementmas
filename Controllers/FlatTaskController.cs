using System.Linq;
using System.Threading.Tasks;
using FlatManagement.Data;
using FlatManagement.Data.Tasking;
using FlatManagement.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FlatManagement.Controllers {
    public class FlatTaskController : Controller {
        private readonly ApplicationDbContext _context;
        private readonly IFlatTaskRepository _repository;
        private readonly IUserUtility _userUtility;

        public FlatTaskController(IFlatTaskRepository repository, ApplicationDbContext context, IUserUtility userUtility) {
            _context = context;
            _repository = repository;
            _userUtility = userUtility;
        }

        // GET: FlatTask
        public async Task<IActionResult> Index() {
            ViewBag.currentUserId = await _userUtility.GetUserIdAsync(this.User);
            var tasks = _repository.FindAllAsync();
            return View(await tasks);
        }

        // GET: FlatTask/Create
        public IActionResult Create() {
            return View();
        }

        // POST: FlatTask/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FlatTaskId,Description,Deadline,IsProgressive,Progress,FlatId,UserId")] FlatTask flatTask) {
            if (ModelState.IsValid) {
                _context.Add(flatTask);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(flatTask);
        }

        // GET: FlatTask/Edit/5
        public async Task<IActionResult> Edit(int? id) {
            if (id == null) {
                return NotFound();
            }

            var flatTask = await _context.FlatTasks.FindAsync(id);
            if (flatTask == null || !(await _userUtility.IsAuthorizedToAsync(this.User, flatTask))) {
                return NotFound();
            }

            var flatmates = await _repository.FindFlatMatesAsync(await _repository.FindFlatIdAsync(flatTask));
            ViewBag.Flatmates = flatmates;
            return View(flatTask);
        }

        // POST: FlatTask/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FlatTaskId,Name,Description,Deadline,Progress,FlatUserId")] FlatTask flatTask) {
            if (id != flatTask.FlatTaskId || !(await _userUtility.IsAuthorizedToAsync(this.User, flatTask))) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                await _repository.UpdateTaskAsync(flatTask);
                return RedirectToAction(nameof(Index));
            }
            return View(flatTask);
        }

        // GET: FlatTask/Delete/5
        public async Task<IActionResult> Delete(int? id) {
            if (id == null) {
                return NotFound();
            }

            var flatTask = await _context.FlatTasks
                .Include(f => f.Flat)
                .FirstOrDefaultAsync(m => m.FlatTaskId == id);
            if (id != flatTask.FlatTaskId || !(await _userUtility.IsAuthorizedToAsync(this.User, flatTask))) {
                return NotFound();
            }

            return View(flatTask);
        }

        // POST: FlatTask/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id) {
            var flatTask = await _context.FlatTasks.FindAsync(id);
            if (!(await _userUtility.IsAuthorizedToAsync(this.User, flatTask))) {
                return NotFound();
            }
            _context.FlatTasks.Remove(flatTask);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: FlatTask/ChangeType/5
        public async Task<IActionResult> ChangeType(int id) {
            await _repository.ChangeTaskTypeAsync(id);
            return Redirect(Request.Headers["Referer"].ToString());
        }

        // GET: FlatTask/MarkAsDone/5
        public async Task<IActionResult> MarkAsDone(int id) {
            await _repository.MarkFlatTaskAsDone(id);
            return RedirectToAction(nameof(Index));
        }

        private bool FlatTaskExists(int id) {
            return _context.FlatTasks.Any(e => e.FlatTaskId == id);
        }
    }
}