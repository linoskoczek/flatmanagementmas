#!/bin/bash
DB_FILE="app.db"
MIGRATIONS_NO=$(dotnet ef migrations list | grep -e "^$(date +%Y | cut -c 1,2)" | wc -l)

echo "There are $MIGRATIONS_NO migrations to remove..."

echo "Removing $DB_FILE..."
rm $DB_FILE

for (( i=0; i<$MIGRATIONS_NO; i++ ))
do
    echo [$(($i+1))/$MIGRATIONS_NO]
    dotnet ef migrations remove
done

dotnet ef migrations add InitialMigration
dotnet ef database update